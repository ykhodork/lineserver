# LineServer

In this exercise, you will build and document a system that is capable of serving lines out of a file to network clients.

You may do so in any language (though a JVM-language is preferred).

You may use any reference and any (open-source) software you can find to help you build this system, so long as you document your use.  However, you should not actively collaborate with others, as the purpose of this exercise is for you to demonstrate your own skills as an engineer.

# Specification

Your system should act as a network server that serves individual lines of an immutable text file over the network to clients using a simple protocol.

Clients open a TCP socket connection to the server, and then may (or may not) send a series of commands. Each command is terminated with a newline ('\n') character. The server responds according to the last command sent by the client.

	GET nnnn

        If nnnn is a valid line number for the given text file, return "OK\r\n"
        and then the nnnn-th line of the specified text file.

        If nnnn is not a valid line number for the given text file, return
        "ERR\r\n".

        The first line of the file is line 1 (not line 0).

    QUIT
    
        Disconnect client

    SHUTDOWN
    
        Shutdown the server

The server should listen for connections on TCP port 10533.

Your server must support a single client at a time. Your server may optionally support multiple simultaneous clients.

The system should perform well for small and large files.

The system should perform well as the number of GET requests per unit time increases.

You may pre-process the text file in any way that you wish so long as the server behaves correctly.

The text file will have the following properties:
* Each line is terminated with a newline ("\n").
* Any given line will fit into memory.
* The line is valid ASCII (e.g. not Unicode).

# Example

Suppose the given file is:

    the
    quick brown
    fox jumps over the
    lazy dog

Then you could imagine the following transcript with your server:

    C => GET 1
    S <= OK
    S <= the
    C => GET -3
    S <= ERR
    C => GET 4
    S <= OK
    S <= lazy dog
    C <= QUIT

# Execution environment

You may assume that your system will execute on a machine at least comparable
to an EC2 64-bit m3.large instance running Ubuntu 14.04.

An m3.large instance has:
* 7.5 GB of memory
* Two 64-bit cores
* an 8 GB of root partition
* a 32 GB SSD drive mounted under /mnt

The entire machine is at your disposal.

# What to submit

The top-level directory of your submission should documentation for your system, a 'tools' subdirectory, and the source code for the system itself.

The 'tools' subdirectory should contain shell scripts to build and run your system. We will probably run these scripts manually, but it would be nice if they worked without manual intervention.

* build.sh - A script that can be invoked to build your system. This script may exit without doing anything if your system does not need to be compiled.  You may invoke another tool such as Maven, Gradle, Ant, GNU make, etc. with this script. You may download and install any libraries or other programs you feel are necessary to help you build your system.

* run.sh - A script that takes a single command-line parameter which is the name of the file to serve.  Ultimately, it should start the server you have built.

Your documentation should include a README in the top-level:

* README - A text file that answers the following questions:
    * How does your system work? (if not addressed in comments in source)
		The server is a Netty based telnet-like server that supports the specified commands.
		A request handler talks to an in-memory LRU cache and an in-memory index (in case of cache miss).
		The LRU cache is limited at 1GB. Since there's no information about a typical text line that is an arbitrary value that allows for ~4M lines of length 120 characters.
		The index is a line# -> offset-in-file mapping, limited at 2GB which allows for ~134M buckets (ignoring Java map overhead).
		If there are more than that amount of lines in the file, the index contains the offset of the first line in the bucket.
		
		A buffered version of Java's RandomAccessFile (3rd party library) is used to retrieve lines from disk.
		There are much faster text readers but this was an easily available (sequential) reader that provides a file pointer (which is used for indexing) and reasonably fast random access to a file.
		It's performance benefits are not constant, as it stalls after a certain amount of sequential lines read.
		This mostly manifests in the initial indexing stage (e.g. 5MB of text takes about a minute to index),
		for that reason I didn't bother implementing a more efficient disk IO routine.
		Better implementations would include:
			- various buffered readers for the initial indexing
			- multithreaded non synchronized random reads (e.g. splitting the file into chunks)
			- importing the text into a database.

    * How will your system perform as the number of requests per second increases?
    * How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file (hardware permitting)?
    
    	I haven't done any profiling to reduce scope, but Netty is (quantifiably speaking) super fast,
    	and if the served file fits in the index (bucket size == 1), I estimate in the thousands of requests per second.
    	This low performance is because of the synchronized single point of disk IO implementation.
    	Also depends of course on requested line number distribution (cache hits).

    	
    * What documentation, websites, papers, etc did you consult in doing this assignment?

    	http://netty.io/wiki/user-guide-for-5.x.html
    	http://stackoverflow.com/questions/5614206/buffered-randomaccessfile-java 
		http://guava-libraries.googlecode.com/files/ConcurrentCachingAtGoogle.pdf
		http://maven.apache.org/plugins/maven-resources-plugin/examples/include-exclude.html
    	Javadocs for most non-trivial classes used
    	
    
    * What third-party libraries or other tools does the system use?
		Maven - for build and deps  (http://maven.apache.org/plugins/maven-resources-plugin/examples/include-exclude.html)
    	ConcurrentLinkedHashmap - Used for an LRU cache on the server (https://code.google.com/p/concurrentlinkedhashmap/)
    	Netty - for easy server IO implementation (www.netty.io)
    	BufferedRandomAccessFile - https://code.google.com/p/jmzreader/wiki/BufferedRandomAccessFile
    	
    * How long did you spend on this exercise?
    	~ 10 hours (~6 of which spent on documentation and familiarizing with Netty, Maven plugins and researching an efficient way to do IO).

The remainder of the files in your tree should be the source-code for your system.

# How to submit

If you have used a distributed version control system (such as Git, Mercurial), please sign up for an account on http://bitbucket.org/ and push your repository there.  As BitBucket permits private repsositories, we recommend that your new repository be private. Give the user "hargettp" read-only access to this new repository.

Otherwise, please package everything up as a .zip file.

If you have used some other version control system (such as Subversion, Perforce, CVS, etc), please include an output of the revision history log named "CHANGES" in the top-level directory.