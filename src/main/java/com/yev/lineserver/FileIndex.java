/**
 *
 */
package com.yev.lineserver;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import uk.ac.ebi.pride.tools.braf.BufferedRandomAccessFile;

import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap;
import com.googlecode.concurrentlinkedhashmap.EntryWeigher;

/**
 * @author ykhodork
 *
 */
public class FileIndex {

    private Long numberOfLines = 0L;
    private BufferedRandomAccessFile fileHandler;
    private boolean isReady = false;

    // An index of line number -> offset in the file
    private ConcurrentNavigableMap<Long, Long> index;

    // XXX: there are more appropriate caching mechanisms of course but scope is
    // reduced for time limit
    private ConcurrentMap<Long, String> LRU;
    private static final int LRU_SIZE_BYTES = 1024 * 1024 * 1024; // 1 GB

    // 2 GB divided by an entry size of <long,long> (ignoring map overhead for now)
    private static final int MAX_INDEX_BUCKETS = (int) (2L * LRU_SIZE_BYTES / (2L * 8L));

    // Implicit init to 1 which is the case of line numbers < max index buckets
    private long indexingBucketSize = 1;

    // Indexing a large file might take a while, so we don't block main thread
    private ExecutorService indexingWorker;
    private Future<Boolean> indexingJob;
    private final Callable<Boolean> indexingTask = new Callable<Boolean>() {

        @Override
        public Boolean call() {
            System.out.println("Indexing file...");

            try {
                long fileSize = fileHandler.length();
                // count lines in file
                String line;
                int cacheWarmup = 1;

                // Initial read all lines, count and index. A lousy attempt to
                // warm up cache is also included in this step
                while (fileHandler.getFilePointer() < fileSize) {

                    // We use this opportunity to fill up index for the case
                    // where number of lines will be less than or equal to
                    // number of index buckets, then we won't have to re-index
                    if (index.size() < MAX_INDEX_BUCKETS) {
                        index.put(numberOfLines, fileHandler.getFilePointer());
                    }

                    line = fileHandler.getNextLine();

                    // Warm up cache in a logarithmic regression manner
                    if (numberOfLines >= cacheWarmup) {
                        LRU.put(numberOfLines, line);
                        cacheWarmup *= 2;
                    }

                    numberOfLines++;
                    //System.out.println("Read line " + numberOfLines);
                }
                fileHandler.seek(0);

                // If number of lines is bigger than max index buckets, we need
                // to re-index
                if (numberOfLines > MAX_INDEX_BUCKETS) {
                    indexingBucketSize = numberOfLines / MAX_INDEX_BUCKETS;
                    index.clear();
                    for (int i = 0; i < MAX_INDEX_BUCKETS; i++) {
                        // FFWD to beginning of next bucket
                        fetchNthLine(indexingBucketSize);
                        // Index line number of first line in bucket and its offset
                        index.put(indexingBucketSize * i, fileHandler.getFilePointer());

                    }
                }

                fileHandler.seek(0);

            } catch (IOException ioe) {
                System.out.println("Indexing file FAILED!");
                return false;
            }

            isReady = true;

            System.out.println("Indexing file DONE!");
            indexingWorker.shutdown();
            return true;
        }
    };

    /**
     * This method seeks to the specified line and returns it, leaving the file
     * handler at beginning of next line
     *
     * @param l
     *            line number of the line to return
     * @return String line
     */
    private String fetchNthLine(long l) {
        try {
            // FF file pointer to the beginning of Nth line
            for (long i = 0; i < l - 1; i++) {
                fileHandler.getNextLine();
            }

            return fileHandler.getNextLine();
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * This method seeks to the specified offset, gets Nth line and returns it,
     * rewinding the file to start.
     *
     * @param offset
     *            - byte offset to start reading at
     * @param n
     *            - Nth line to return
     * @return String line
     */
    private String fetchNthLineAndRewind(long offset, long n) {

        String result;
        try {
            fileHandler.seek(offset);
            result = fetchNthLine(n);
            fileHandler.seek(0);

        } catch (IOException e) {
            result = null;
        }
        return result;

    }

    public FileIndex(String fileName) throws IOException {

        fileHandler = new BufferedRandomAccessFile(fileName, "r", 10 * 1024 * 1024);

        // init index
        index = new ConcurrentSkipListMap<Long, Long>();

        // init LRU cache
        LRU = createCache();

        indexingWorker = Executors.newSingleThreadExecutor();
        indexingJob = indexingWorker.submit(this.indexingTask);

    }

    /**
     * Blocking method to wait for indexing to finish
     * @return true iff indexing completed successfully
     */
    public boolean waitIndexingComplete() {
        try {
            return indexingJob.get();

        } catch (InterruptedException | ExecutionException ee) {
            return false;
        }
    }

    /**
     * Creates an LRU ConcurrentMap limited at 1GB
     */
    private ConcurrentMap<Long, String> createCache() {
        EntryWeigher<Long, String> memoryUsageWeigher = new EntryWeigher<Long, String>() {
            @Override
            public int weightOf(Long K, String V) {
                return 8 + V.length() * 2; // size of entry is approx. size of long and (characters * 2)
            }
        };
        ConcurrentMap<Long, String> cache = new ConcurrentLinkedHashMap.Builder<Long, String>()
                .maximumWeightedCapacity(LRU_SIZE_BYTES).weigher(memoryUsageWeigher).build();

        return cache;
    }

    /**
     * This is the public interface to get a line at specified position from the file
     *
     * @param lineNumber
     *            - the desired line number
     * @return String the requested line
     */
    public String getLine(long lineNumber) {

        if (lineNumber > numberOfLines || lineNumber < 1) {
            return null;
        }

        lineNumber -=1; //index is 0-based
        // Check cache
        String result = LRU.get(lineNumber);

        if (!isReady) {
            // Even though indexing not complete, line may already be cached by
            // the lousy warm up
            return result;
        }

        // Cache miss, lookup by index
        if (result == null) {

            Long offset = index.get(lineNumber);
            // Requested line number is not directly indexed, so get its bucket offset
            if (offset == null) {
                offset = index.floorEntry(lineNumber).getValue();
            }

            // TODO: come up with a concurrent file read mechanism
            synchronized (fileHandler) {
                result = fetchNthLineAndRewind(offset, lineNumber % indexingBucketSize);
            }
        }

        return result;
    }

    public long getNumberOfLines() {
        return numberOfLines;
    }

    public boolean isReady() {
        return isReady;
    }

}
