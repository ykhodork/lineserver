package com.yev.lineserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.io.IOException;

/**
 * Hello Lineserver
 *
 */
public class LineServer {
    public static final int SERVER_PORT = 10533;

    LineServer self;
    private FileIndex fileIndex;
    ChannelFuture serverChannelFuture;
    EventLoopGroup bossGroup;
    EventLoopGroup workerGroup;


    public LineServer(String fileName) throws IOException {
        //index file for fast fetching
        fileIndex = new FileIndex(fileName);

        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();

        // We give handlers a reference to the server object for the shutdown command
        self = this;
    }

    public void run() {

        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap
                    .group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        private final StringDecoder DECODER = new StringDecoder();
                        private final StringEncoder ENCODER = new StringEncoder();

                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();

                            // Add the text line codec combination first,
                            pipeline.addLast(new DelimiterBasedFrameDecoder(Integer.MAX_VALUE, Delimiters
                                    .lineDelimiter()));
                            pipeline.addLast(DECODER);
                            pipeline.addLast(ENCODER);

                            // and then business logic.
                            pipeline.addLast(new LineServerHandler(self, fileIndex));
                        }
                    }).option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true)
                    .childOption(ChannelOption.TCP_NODELAY, true);

            // Bind and start accepting incoming connections.
            this.serverChannelFuture = serverBootstrap.bind(SERVER_PORT).sync();

        } catch (InterruptedException ie) {

        }
    }

    public static void main(String[] args) {

        String file = "input";
        if (args.length > 0) {
            if (args.length > 0) {
                file = args[0];
            }
        }

        try {
            new LineServer(file).run();
        } catch (IOException e) {
            System.out.println(String.format("File %s not found! Shutting down.", file));
        }
    }

    public void shutdown() {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
            this.serverChannelFuture.channel().closeFuture().cancel(true);

    }

    public boolean addSupportedCommand(Command cmd) {
        //TODO: implement
        throw new UnsupportedOperationException();
    }
}
