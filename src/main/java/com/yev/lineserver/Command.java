/**
 *
 */
package com.yev.lineserver;

/**
 * Interface for commands that are implemented by the server
 *
 * Extending supported commands is not implemented
 *
 * @author ykhodork
 *
 */
public interface Command {
    public void execute();

}
