package com.yev.lineserver;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class LineServerHandler extends SimpleChannelInboundHandler<String> {

    private FileIndex index;
    private LineServer host;

    public LineServerHandler(LineServer server, FileIndex fileIndex) {
        index = fileIndex;
        host = server;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        // Send greeting for a new connection.
        new Hello(ctx).execute();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        cause.printStackTrace();
        try {
            ctx.writeAndFlush("ERR");
        } finally {
            // Close the connection when an exception is raised.
            ctx.close();
        }
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        try {
            Command cmd;
            if (msg.contains("GET")) {

                String[] params = msg.split("GET");
                String param;
                if (params.length > 1) {
                    param = params[1].trim();
                } else {
                    param = null;
                }
                cmd = new Get(ctx, param);

            } else if (msg.contains("QUIT")) {
                cmd = new Quit(ctx);

            } else if (msg.contains("SHUTDOWN")) {
                cmd = new Shutdown(ctx);

            } else if (msg.isEmpty()) {
                cmd = new Hello(ctx);
            } else {
                cmd = new Error(ctx);
            }

            cmd.execute();

        } finally {
            ReferenceCountUtil.release(msg);
        }

    }

    class Get implements Command {
        private long lineNumber;
        private ChannelHandlerContext commandContext;

        public Get(ChannelHandlerContext ctx, String lineNumber) {
            try {
                this.lineNumber = Long.decode(lineNumber);
            } catch (NumberFormatException | NullPointerException e) {
                this.lineNumber = -1;
            }
            commandContext = ctx;
        }

        @Override
        public void execute() {
            String line = index.getLine(lineNumber);
            if (line != null) {
                commandContext.write("OK\r\n");
                commandContext.writeAndFlush(line + "\r\n");
            } else {
                commandContext.writeAndFlush("ERR\r\n");
            }

        }
    }

    class Quit implements Command {
        private ChannelHandlerContext commandContext;

        public Quit(ChannelHandlerContext ctx) {
            commandContext = ctx;
        }

        @Override
        public void execute() {
            commandContext.disconnect();
        }

    }

    class Shutdown implements Command {
        private ChannelHandlerContext commandContext;

        public Shutdown(ChannelHandlerContext ctx) {
            commandContext = ctx;
        }

        @Override
        public void execute() {
            commandContext.disconnect();
            host.shutdown();

        }

    }


    class Error implements Command {
        private ChannelHandlerContext commandContext;

        public Error(ChannelHandlerContext ctx) {
            commandContext = ctx;
        }

        @Override
        public void execute() {
            commandContext.writeAndFlush("ERR\r\n");

        }

    }


    // A default NOP command
    class Hello implements Command {
        private ChannelHandlerContext commandContext;

        public Hello(ChannelHandlerContext ctx) {
            commandContext = ctx;
        }

        @Override
        public void execute() {
            try {
                commandContext.write("Greetings from Line Server at " + InetAddress.getLocalHost() + "!\r\n");
                commandContext.writeAndFlush("Today we're serving "
                        + (index.isReady() ? index.getNumberOfLines() : "unknown number of") + " line(s), hot or cold\r\n");
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }

}
