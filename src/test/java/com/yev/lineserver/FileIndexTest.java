package com.yev.lineserver;

import java.io.IOException;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for LineServer
 */
public class FileIndexTest extends TestCase {

    private FileIndex index;
    /**
     * Create the test case
     *
     * @param testName
     *            name of the test case
     */
    public FileIndexTest(String testName) {
        super(testName);

        try {
            index = new FileIndex("testinput");
        } catch (IOException e) {
            throw new AssertionError("Test file not found!");
        }
        index.waitIndexingComplete();
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FileIndexTest.class);
    }


    public void testGetLine() {

        System.out.println(index.getLine(3029));
        Assert.assertTrue(index.getLine(1).equals("The Project Gutenberg EBook of The Complete Works of William Shakespeare, by"));
        Assert.assertTrue(index.getLine(1500).equals("  And arts with thy sweet graces graced be."));
        Assert.assertTrue(index.getLine(3028).equals("    I know not what he shall. God"));
        Assert.assertNull(index.getLine(3029));
        Assert.assertNull(index.getLine(-1));
        Assert.assertNull(index.getLine(0));

    }

    public void testGetNumberOfLines() {

        Assert.assertTrue(index.getNumberOfLines() == 3028L);
    }

}
